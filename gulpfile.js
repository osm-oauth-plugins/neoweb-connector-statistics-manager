const productSlug = "neoweb-connector-";
const pluginSlug = productSlug + "statistics-manager";

const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const cssnano = require( 'gulp-cssnano' );
const postcss = require('gulp-postcss');
const watch = require('gulp-watch');
const zip = require('gulp-zip');
const clean = require('gulp-clean');
const rename = require('gulp-rename');
const ignore = require('gulp-ignore');
const rimraf = require('gulp-rimraf');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('autoprefixer');
const gutil = require('gutil');
const ftp = require('vinyl-ftp');

const source = '.',
    destination = 'C:/docker/wp_local1/wp-content/plugins/' + pluginSlug,
    destination_mac = '/Users/jacomare/Documents/Docker/wordpress/wp-content/plugins/' + pluginSlug;

// Configuration file to keep your code DRY
const cfg = require('./gulpconfig.json');
const paths = cfg.paths;

var localFiles = [
    source + '/**/*',
    '!./{node_modules,node_modules/**/*}',
    '!./.git/**',
    '!./.idea/**',
    '!./_MACOSX/**',
    '!./gulpfile.js',
    '!./package.json',
    '!./package-lock.json',
    '!./config.json',
    '!./gulpconfig.json',
    '!./sass/**',
    '!./src/**',
    '!./source/**',
    '!./webfonts/**',
    '!./*.yml'
];
/*
var user = "thamesridgescout";
var password = "-VHFS$[B2Qr7";
var ftpHost = "thamesridgescouts.org.uk";
const remoteLocation = '/public_html/wp-content/plugins/' + pluginSlug;
*/
//dev
var user = "jp9757@dev.thamesridgescouts.org.uk";
var password = ",pVaj7yB}l6~";
var ftpHost = "dev.thamesridgescouts.org.uk";
const remoteLocation = '/wp-content/plugins/' + pluginSlug;

function getFtpConnection(){
    return ftp.create({
        host: ftpHost,
        port: 21,
        user: user,
        password: password,
        log: gutil.log
    });
}


gulp.task('deploy', function(){
    var conn = getFtpConnection();
    return gulp.src(localFiles, {base: '.', buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
})


// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function() {
    const stream = gulp
        .src(paths.sass + '/*.scss')
        .pipe(
            plumber({
                errorHandler: function(err) {
                    console.log(err);
                    this.emit('end');
                }
            })
        )
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sass({ errLogToConsole: true }))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write(undefined, { sourceRoot: null }))
        .pipe(gulp.dest(paths.css));
    return stream;
});


// Run:
// gulp cssnano
// Minifies CSS files
gulp.task('cssnano', function() {
    return gulp
        .src(paths.css + '/plugin.css')
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(
            plumber({
                errorHandler: function(err) {
                    console.log(err);
                    this.emit('end');
                }
            })
        )
        .pipe(rename({ suffix: '.min' }))
        .pipe(cssnano({ discardComments: { removeAll: true } }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.css));
});

gulp.task('minifycss', function() {
    return gulp
        .src(`${paths.css}/plugin.css`)
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(cleanCSS({ compatibility: '*' }))
        .pipe(
            plumber({
                errorHandler: function(err) {
                    console.log(err);
                    this.emit('end');
                }
            })
        )
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.css));
});

gulp.task('cleancss', function() {
    return gulp
        .src(`${paths.css}/*.min.css`, { read: false }) // Much faster
        .pipe(ignore('plugin.css'))
        .pipe(rimraf());
});

gulp.task('styles', function(callback) {
    gulp.series('sass', 'minifycss')(callback);
});

gulp.task('dev', function() {

    var option, i = process.argv.indexOf("--env");
    let env;
    if (i > -1) {
        env = process.argv[i + 1];
    }

    let env_dest;
    if (env == "mac") {
        env_dest = destination_mac;
    } else {
        env_dest = destination;
    }

    gulp.src(localFiles, {base: source})
        .pipe(watch(localFiles, {base: source}))
        .pipe(gulp.dest(env_dest));
});

gulp.task('buildSrc', function () {
    return gulp.src(localFiles)
        .pipe(gulp.dest('./source/' + pluginSlug + '/'));
});

gulp.task('zip', function () {
    return gulp.src([
        './source/**',
    ], {base: './source/'})
        .pipe(zip(pluginSlug + '.zip'))
        .pipe(gulp.dest('./../'));
});

gulp.task('cleanUp', function () {
    return gulp.src('./source', {read: false})
        .pipe(clean());
});

gulp.task('build', gulp.series('buildSrc', 'zip', 'cleanUp', function (done) {
    done();
}));
