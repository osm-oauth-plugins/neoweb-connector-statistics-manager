<?php


class NeoWeb_Connector_Statistics_Manager_OSM_Endpoints {

    const domain = "https://www.onlinescoutmanager.co.uk";
    const authorize = self::domain . '/oauth/authorize';
    const token = self::domain . '/oauth/token';
    const resources = self::domain . '/oauth/resource';

	//Custom Data
	const read_custom_data = self::domain . '/ext/customdata/?action=getStructureSettings&section_id=%s';
	const update_custom_additional_data = self::domain . '/ext/customdata/?action=updateColumn&section_id=%s';

    //Members
    const create_new_member = self::domain . '/ext/members/contact/actions/?action=newMember';
    const update_member_data = self::domain . '/ext/customdata/?action=update&section_id=%s';
    const getAllMembers = self::domain . '/users.php?action=getUserDetails&sectionid=%s&termid=%s&dateFormat=generic&section=%s';

    //Program & Events
    const getProgramData = self::domain . '/programme.php?action=getProgramme&sectionid=%s&termid=%s&eveningid=%s';
    const getEventSummary = self::domain . '/users.php?action=getNextThings&sectionid=%s&termid=%s';
    const getProgramSummary = self::domain . '/programme.php?action=getProgrammeSummary&sectionid=%s&termid=%s';
	const getAllEventData = self::domain . '/ext/events/summary/?action=get&sectionid=%s&termid=%s';
	const getAllProgramData = self::domain . '/ext/programme/?action=getProgrammeSummary&sectionid=%s&termid=%s';

    //Sections & Dashboards
    const getSectionAPIAccess = self::domain . '/ext/settings/access/?action=getAPIAccess&sectionid=%s'; // possibly obsolete
    const getSectionDataAndUserRoles = self::domain . '/api.php?action=getUserRoles'; // possibly obsolete
    const getAllTerms = self::domain . '/api.php?action=getTerms'; // possibly obsolete
    const getGroupDashboard = self::domain . '/ext/dashboard/?action=getGroupDashboard'; //possibly obsolete

    //Patrol
    const getPatrolPoints = self::domain . '/ext/members/patrols/?action=getPatrolsWithPeople&sectionid=%s&termid=%s';

    //Badges
    const getBadgesByPerson = self::domain . '/ext/badges/badgesbyperson/?action=loadBadgesByMember&sectionid=%s&term_id=%s&section=%s';

    //Statistics
    const getSectionNumbers = self::domain . '/ext/discounts/?action=getNumberSections&sectionid=%s&code=%s';
    const getSectionBadgeDrilldown = self::domain . '/ext/discounts/?action=getBadgeDrilldown&sectionid=%s&section=%s&code=%s';
    const getSectionSizes = self::domain . '/ext/discounts/?action=getSizes&sectionid=%s&code=%s';
    const getSectionAges = self::domain . '/ext/discounts/?action=getAges&sectionid=%s&code=%s';
    const getSectionAgesLeaders = self::domain . '/ext/discounts/?action=getAges&leadersonly=true&sectionid=%s&code=%s';
    const getSectionGrowth = self::domain . '/ext/discounts/?action=getGrowth&type=members&sectionid=%s&code=%s';
    const getWaitingListGrowth = self::domain . '/ext/discounts/?action=getGrowth&type=waiting&sectionid=%s&code=%s';
    const getLeaderGrowth = self::domain . '/ext/discounts/?action=getGrowth&type=leaders&sectionid=%s&code=%s';
    const getRetention = self::domain . '/ext/discounts/?action=getRetention&sectionid=%s&code=%s';
    const getNumberEvents = self::domain . '/ext/discounts/?action=getNumberEvents&sectionid=%s&code=%s';

    const getDistricts = self::domain . '/ext/discounts/?action=getUptake&code=70&sectionid=%s';


    /**
     * NeoWeb_OSM_Endpoints formatEndPoint.
     * Arguments should be sent in this order omitting what ever is not required
     * SectionID, TermID, EveningID, Section, Code
     *
     * @param $endPointSelector
     * @param string $arg0
     * @param string $arg1
     * @param string $arg2
     * @return string
     */
    public function formatEndPoint($endPointSelector, $arg0 = "", $arg1 = "", $arg2 = ""): string
    {
        return sprintf($endPointSelector, $arg0, $arg1, $arg2);
    }
}

