<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    NeoWeb_Connector_Statistics_Manager
 * @subpackage NeoWeb_Connector_Statistics_Manager/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
