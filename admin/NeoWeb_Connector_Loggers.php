<?php


class NeoWeb_Connector_Loggers {

	/**
	 * The unique path to the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_path    The string used to uniquely identify this plugin.
	 */
	protected string $plugin_path;

	/**
	 * __constructor.
	 *
	 * @param string $plugin_path
	 */
	public function __construct( string $plugin_path ) {
		$this->plugin_path = $plugin_path;
	}

	/**
	 * @return string
	 */
	public function get_plugin_path(): string {
		return $this->plugin_path;
	}

	public function error_logger ( $log )  {
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, true ) );
		} else {
			error_log( $log );
		}
	}

	public function debug_logger ($message) {
		if ( ! file_exists( $this->get_plugin_path() . 'logs/osmDebugLogs/' ) ) {
			mkdir( $this->get_plugin_path() . 'logs/osmDebugLogs/', 0777, true );
		}

		$debugFilePath = $this->get_plugin_path() . 'logs/osmDebugLogs/osm_debug_log_' . date( "d-M-Y" ) . '.log';

		$content = "[" . date("d-m-Y h:i:sa") . "] " . PHP_EOL;
		if ( is_array( $message ) || is_object( $message ) ) {
			$content .= "[--LOG MESSAGE--] " . print_r($message, true) . PHP_EOL;
		} else {
			$content .= "[--LOG MESSAGE--] " . $message . PHP_EOL;
		}

		file_put_contents($debugFilePath, $content, FILE_APPEND | LOCK_EX);
	}

	public function transient_logger ($function, $url, $args, $response, $readTransient) {

		if (!file_exists($this->get_plugin_path() . 'logs/osmTransientDebugLogs/'. $function)) {
			mkdir($this->get_plugin_path() . 'logs/osmTransientDebugLogs/'.$function, 0777, true);
		}

		$debugFilePath = $this->get_plugin_path() . 'logs/osmTransientDebugLogs/' . $function . '/osm_transient_debug_log_'.date("d-M-Y").'.log';
		if ($readTransient) {
			$content = "[READ USING TRANSIENT CACHE]" . PHP_EOL;
			$content .= "[" . date("d-m-Y h:i:sa") . "] " . $url . PHP_EOL;
		} else {
			$content = "[DATA RECEIVED FROM OSM]" . PHP_EOL;
			$content .= "[" . date("d-m-Y h:i:sa") . "] " . $url . PHP_EOL;
		}

		//Obfuscate the user password before writing to the log
		if (isset($args['body']['password']) ) {
			$args['body']['password'] = "#################### Password Obfuscated ################";
		}

		$content .= "[--API ARGUMENTS--] " . print_r($args, true) . PHP_EOL;

		$msg = wp_remote_retrieve_body( $response );
		if (empty($msg)) {
			$msg = $response;
		}
		if ( is_array( $msg ) || is_object( $msg ) ) {
			$content .= "[--API RESPONSE--] " . print_r($msg, true) . PHP_EOL;
		} else {
			$content .= "[--API RESPONSE--] " . $msg . PHP_EOL;
		}

		$msg = wp_remote_retrieve_header( $response, 'X-RateLimit-Limit' );

		if ( is_array( $msg ) || is_object( $msg ) ) {
			$content .= "[--API X-RateLimit-Limit RESPONSE--] " . print_r($msg, true) . PHP_EOL;
		} else {
			$content .= "[--API X-RateLimit-Limit RESPONSE--] " . $msg . PHP_EOL;
		}
		$msg = wp_remote_retrieve_header( $response, 'X-RateLimit-Remaining' );
		if ( is_array( $msg ) || is_object( $msg ) ) {
			$content .= "[--API X-RateLimit-Remaining RESPONSE--] " . print_r($msg, true) . PHP_EOL;
		} else {
			$content .= "[--API X-RateLimit-Remaining RESPONSE--] " . $msg . PHP_EOL;
		}
		$msg = wp_remote_retrieve_header( $response, 'X-RateLimit-Reset' );
		if ( is_array( $msg ) || is_object( $msg ) ) {
			$content .= "[--API X-RateLimit-Reset RESPONSE--] " . print_r($msg, true) . PHP_EOL;
		} else {
			$content .= "[--API X-RateLimit-Reset RESPONSE--] " . $msg . PHP_EOL;
		}

		file_put_contents($debugFilePath, $content, FILE_APPEND | LOCK_EX);

	}

	public function recursiveRemove($dir) {
		$structure = glob(rtrim($dir, "/").'/*');
		if (is_array($structure)) {
			foreach($structure as $file) {
				if (is_dir($file)) $this->recursiveRemove($file);
				elseif (is_file($file)) unlink($file);
			}
		}
		$dir_handle = "";

		if (is_dir($dir))
			$dir_handle = opendir($dir);
		if (!$dir_handle)
			return;

		rmdir($dir);
	}

}