(function( $ ) {
	'use strict';

	$(function() {

		const productSlug = "neoweb_connector_statistics_manager";

		$('#'+ productSlug +'triggerAPITest').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_api_test_' + productSlug
			};

			$.post(ajaxurl, data, function(data) {
				console.log(data);
				alert(data['status']);
			});
		});

		$('#'+ productSlug +'triggerLogRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_log_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'triggerTransientLogRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_transient_log_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'triggerCacheRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_cache_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_register').on('click', function() {
			$(this).prop('disabled', true);
			const data = {
				'action': 'trigger_create_licence_key_request_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_activate').on('click', function() {
			$(this).prop('disabled', true);
			const data = {
				'action': 'trigger_activate_licence_key_request_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('div.disabled').each(function () {
			$(this).find('input:radio:not(:checked)').prop('disabled', true);
		});

	});

})( jQuery );
