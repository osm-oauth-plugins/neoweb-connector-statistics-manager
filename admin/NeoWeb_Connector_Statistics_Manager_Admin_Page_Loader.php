<?php


class NeoWeb_Connector_Statistics_Manager_Admin_Page_Loader {

	private NeoWeb_Connector_Loggers $logger;
	private NeoWeb_Connector_Licence_Manager $licenceManager;
	private NeoWeb_Connector_Statistics_Manager_Auth_Caller $oAuthCaller;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 *
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-statistics-manager');
		$this->licenceManager = new NeoWeb_Connector_Licence_Manager();
		$this->oAuthCaller = new NeoWeb_Connector_Statistics_Manager_Auth_Caller();
	}

	public function loadAdminPages () {

		$productPageLoader = new NeoWeb_Connector_Register_Product_Pages();
		$productPageLoader->registerProductPages();
		$productPageLoader->registerProductPageLogos();

		//Add application licence option page and fields
		$applicationLicencePage = new NeoWeb_Connector_Register_Licence_Page();
		$applicationLicencePage->registerLicencePersonalDetailsFields();
		$applicationLicencePage->registerLicenceProductDetailsFields();

		$licenseCheck = $this->licenceManager->checkLicenceKey();
		if ($licenseCheck) {

			//Register Protected Product Pages & Logos(Needs a licence)
			$productPageLoader->registerProtectedProductPages();
			$productPageLoader->registerProtectedProductPageLogos();

			//Add OSM authentication settings page and fields
			$applicationSettingsPage = new NeoWeb_Connector_Register_OSM_App_Settings_Page();
			$applicationSettingsPage->registerGenericInstructions();
			$applicationSettingsPage->registerApplicationFields();

			/**
			 * Check we have app details saved and that we have an access token from a previous authentication call,
			 * then try to renew the accessToken if needed.
			 **/
			if (get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_client_id", "option") &&
			    get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_secret", "option")) {

				$accessToken = $this->oAuthCaller->get_access_token();
				if (!empty($accessToken)) {
					$osm_oauth_accessToken = $accessToken;
					$counties = $this->oAuthCaller->getAvailableCounties();
                    $countiesMetaData = $this->oAuthCaller->getTransientMetaData('resourceData');

					//Register Plugin Resources Option Page
					$resourcesPage = new NeoWeb_Connector_Register_Resources_Page();
					$resourcesPage->prepareResourcesFields($counties, $countiesMetaData);

					$settingsPage = new NeoWeb_Connector_Statistics_Manager_Settings_Page();
					$settingsPage->registerSettingPage();
					$settingsPage->registerPluginLogo();
					foreach ($counties as $countyID => $countyData) {
						foreach ( $countyData as $county ) {
							$districts = $this->oAuthCaller->getAvailableDistricts($county['section_id']);
							$districtsMetaData = $this->oAuthCaller->getTransientMetaData('district_data_'. $county['section_id']);
							$settingsPage->registerSettingsPageDistricts($districts, $districtsMetaData);

							//Discount = 70
							$settingsPage->registerSettingsPageFields( $county['section_id'], "70" , $countiesMetaData);
						}
					}
				}
			}

			$applicationDebugPage = new NeoWeb_Connector_Register_Debug_Cache_Page();
			$applicationDebugPage->registerDebugSettingsPageDebugFields();

			$applicationSupportPage = new NeoWeb_Connector_Register_Support_Page();
			$applicationSupportPage->registerDefaultSupportFields();
			$applicationSupportPage->registerPluginSupportFields();
		}
	}
}