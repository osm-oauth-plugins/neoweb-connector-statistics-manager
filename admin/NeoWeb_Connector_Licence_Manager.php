<?php


class NeoWeb_Connector_Licence_Manager {

	private string $licenceCreationKey = '60044807dd1cd9.07778865';
	private string $licenceValidationKey = '60044807dd1d40.88556940';
	private string $licencingServer = "https://scoutsuk.org";
	private $activeDomain;

	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * NeoWeb_Connector_Register_Licence_Page constructor.
	 *
	 */
	public function __construct()
	{

		$this->plugin_data = get_option('neoweb-connector-statistics-manager');
		$this->activeDomain = $_SERVER['SERVER_NAME'];

		//If we have a licence key already - Lets check what we have in case it has been changed
		$licenceKey = get_field($this->get_plugin_data('pluginSlug') . '_licence_key', 'option');
		if (isset($licenceKey)) {
			$this->checkLicenceKey();
		}
	}

	public function createLicenceKey ($licenceRequestBody): array {

		$licenceRequestBody['secret_key'] = $this->licenceCreationKey;
		return wp_remote_post(add_query_arg($licenceRequestBody, $this->licencingServer), array('timeout' => 20, 'sslverify' => false));

	}

	public function activateLicenceKey ($licenceRequestBody): array {
		$licenceRequestBody['secret_key'] = $this->licenceValidationKey;
		$licenceRequestBody['license_key'] = get_field($this->get_plugin_data('pluginSlug') . '_licence_key', 'option');

		$query = esc_url_raw(add_query_arg($licenceRequestBody, $this->licencingServer));
		return wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

	}

	public function deactivateLicenceKey() {
		//TODO
	}

	public function checkLicenceKey (): bool {

		$licenceRequestBody = array(
			'slm_action' => 'slm_check',
			'secret_key' => $this->licenceValidationKey,
			'license_key'=> get_field($this->get_plugin_data('pluginSlug') . '_licence_key', 'option'),
			'registered_domain' =>  $this->activeDomain
		);

		$response = wp_remote_get(add_query_arg($licenceRequestBody, $this->licencingServer), array('timeout' => 20, 'sslverify' => false));

		$responseBody = json_decode(wp_remote_retrieve_body($response), true);

		if (isset($responseBody['status'])) {

			$licenceStatus = $responseBody['status'];
			$registeredDomains = $responseBody['registered_domains'];

			$domainCheck = false;
			foreach ($registeredDomains as $registered_domain) {
				if ($registered_domain['registered_domain'] == $this->activeDomain) {
					$domainCheck = true;
				}
			}

			if ($licenceStatus == "active" && $domainCheck) {

				$currentStatus = get_field($this->get_plugin_data('pluginSlug') . '_licence_status', 'option');
				//The status has been changed lest update the admin screens
				if ($currentStatus != $licenceStatus)
					update_field($this->get_plugin_data('pluginSlug') . '_licence_status', ucfirst($licenceStatus), "option");

				return true;
			} else {
				//The licence key has been cancelled...
				update_field($this->get_plugin_data('pluginSlug') . '_licence_status', ucfirst($licenceStatus), "option");
				return false;
			}

		} else {

			if ($responseBody['error_code'] == 60) {
				//The licence key we have is invalid or does not exists, remove it so that we can try again
				update_field($this->get_plugin_data('pluginSlug') . '_licence_key', "", "option");
				update_field($this->get_plugin_data('pluginSlug') . '_licence_status', "Please request a licence key from NeoWeb using the button below...", "option");
			}

			return false;
		}
	}

	public function getLicenceStatus(): string {
		$licenceKey = get_field($this->get_plugin_data('pluginSlug') . '_licence_key', 'option');
		if (isset($licenceKey)) {
			$licenceRequestBody = array(
				'slm_action' => 'slm_check',
				'secret_key' => $this->licenceValidationKey,
				'license_key'=> $licenceKey,
				'registered_domain' =>  $this->activeDomain
			);

			$response = wp_remote_get(add_query_arg($licenceRequestBody, $this->licencingServer), array('timeout' => 20, 'sslverify' => false));

			$responseBody = json_decode(wp_remote_retrieve_body($response), true);

			if (isset($responseBody['status'])) {
				return ucfirst($responseBody['status']);
			} else {
				return ucfirst($responseBody['message']);
			}
		} else {
			return "Please request a licence key from NeoWeb using the button below...";
		}


	}

	public function updateLicenceStatus($status) {
		echo $status;
	}

}