<?php


class NeoWeb_Connector_Statistics_Manager_Settings_Page
{
	private string $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 */
	public function __construct()
	{
		$this->plugin_data = get_option('neoweb-connector-statistics-manager');
		$this->pageID = $this->get_plugin_data('pluginSlug') . '-settings';
	}

    public function registerSettingPage() {
	    $img_folder_path = plugin_dir_url( dirname(__FILE__) );
        if( function_exists('acf_add_options_page') ):

	        acf_add_options_page(array(
		        'page_title' => $this->get_plugin_data('pluginName'),
		        'menu_title' => str_replace("NeoWeb Connector - ", "", $this->get_plugin_data('pluginName')),
		        'menu_slug' => $this->get_plugin_data('pluginSlug') . '_parent',
		        'capability' => 'manage_options',
		        'position' => '',
		        'parent_slug' => '',
		        'icon_url' => $img_folder_path . '/images/product-icon.png',
		        'redirect' => true,
	        ));

	        acf_add_options_page(array(
                'page_title' => 'Shortcodes & Settings',
                'menu_title' => 'Plugin Settings',
                'menu_slug' => $this->pageID,
                'capability' => 'manage_options',
                'position' => '5',
                'parent_slug' => $this->get_plugin_data('pluginSlug') . '_parent',
                'icon_url' => '',
                'redirect' => false,
                'post_id' => 'options',
                'autoload' => false,
                'update_button' => 'Update',
                'updated_message' => 'Options Updated',
            ));

        endif;
    }

	public function registerPluginLogo() {
		$img_folder_path = plugin_dir_url( dirname(__FILE__) );
		if( function_exists('acf_add_local_field_group') ):
			acf_add_local_field_group(array(
				'key' => 'group_logo' . $this->pageID,
				'title' => 'plugin_logo',
				'fields' => array(
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => -1,
				'position' => 'acf_after_title',
				'style' => 'seamless',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field(array(
				'key' => 'field_' . 'logo_' . $this->pageID,
				'label' => '',
				'name' => 'logo_' . $this->pageID,
				'type' => 'message',
				'message' => '<div class="logoWrapper">
        <img src="' . $img_folder_path . '/images/logo.png"></div>',
				'parent' => 'group_logo' . $this->pageID,
			));
		endif;
	}

    public function registerSettingsPageDistricts($districts, $districtMetaData) {
	    if( function_exists('acf_add_local_field_group') ):
		    acf_add_local_field_group(array(
			    'key' => 'group_' . 'available_districts' . $this->pageID,
			    'title' => 'Available District Filters',
			    'fields' => array(
			    ),
			    'location' => array(
				    array(
					    array(
						    'param' => 'options_page',
						    'operator' => '==',
						    'value' => $this->pageID,
					    ),
				    ),
			    ),
			    'menu_order' => 0,
			    'position' => 'side',
			    'style' => 'default',
			    'label_placement' => 'top',
			    'instruction_placement' => 'field',
			    'hide_on_screen' => '',
			    'active' => true,
			    'description' => '',
		    ));

		    acf_add_local_field(array(
			    'key' => 'field_' . 'district_message' . $this->pageID,
			    'label' => '',
			    'name' => 'district_message' . $this->pageID,
			    'type' => 'message',
			    'message' => 'Please select the district that should be included in the graphs.',
			    'parent' => 'group_' . 'available_districts' . $this->pageID,
			    'wrapper' => array (
				    'width' => '',
				    'class' => 'neowebNotice',
				    'id' => '',
			    ),
		    ));

	        foreach ($districts as $district => $districtData) {

		        acf_add_local_field(array(
			        'key' => 'field_' . 'district' . $this->pageID . str_replace(' ', '', $district),
			        'label' => '',
			        'name' => 'district' . $this->pageID . str_replace(' ', '', $district),
			        'type' => 'true_false',
			        'default_value' => 1,
			        'message' => $district,
			        'parent' => 'group_' . 'available_districts' . $this->pageID,
		        ));

	        }

		    acf_add_local_field(array(
			    'key' => 'field_' . 'district_report_filter_' . $this->pageID,
			    'label' => '',
			    'name' => 'district_report_filter_' . $this->pageID,
			    'type' => 'message',
			    'message' => 'These filters will NOT be applied to the "Member Ages & Leader Ages" reports under the Unfiltered Dashboard Reports section.',
			    'parent' => 'group_' . 'available_districts' . $this->pageID,
			    'wrapper' => array (
				    'width' => '',
				    'class' => 'neowebNotice',
				    'id' => '',
			    ),
		    ));

		    acf_add_local_field(array(
			    'key' => 'field_' . 'district_list_refresh_' . $this->pageID,
			    'label' => 'List of districts will refresh in:',
			    'name' => 'district_list_refresh_' . $this->pageID,
			    'type' => 'message',
			    'message' => $districtMetaData,
			    'parent' => 'available_districts' . $this->pageID
		    ));


	    endif;
    }

    public function registerSettingsPageFields($section_id, $section_code, $groupedSectionsMetaData) {
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_' . 'available_dashboard_reports',
                'title' => 'Available Dashboard Reports (Filtered)',
                'fields' => array(
	                array(
		                'key' => $this->get_plugin_data('pluginSlug') . '_field_section_id_metaData',
		                'label' => 'Data on this page is refreshed every 24 hours.<br/>Time left until next refresh:',
		                'name' => $this->get_plugin_data('pluginSlug') . '_field_section_id_metaData',
		                'type' => 'message',
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => '',
			                'id' => '',
		                ),
		                'message' => $groupedSectionsMetaData,
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_Beavers_badges',
                        'label' => 'Beaver Badges',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
                    array(
                        'key' => 'OSM_Beavers_badges_' . $section_id,
                        'label' => 'Short code',
                        'name' => 'OSM_Beavers_badges_' . $section_id,
                        'type' => 'text',
                        'readonly' => 1,
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'shortCodeCopy',
                            'id' => '',
                        ),
                        'default_value' => '[OSM_Beavers_badges section-id="' . $section_id . '" section-code="' . $section_code . '"]',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    ),
                    array(
                        'key' => 'OSM_Cubs_badges',
                        'label' => 'Cub Badges',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Cub_badges_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Cub_badges_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Cub_badges section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_Scouts_badges',
                        'label' => 'Scout Badges',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Scout_badges_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Scout_badges_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Scout_badges section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_Explorers_badges',
                        'label' => 'Explorer Badges',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Explorer_badges_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Explorer_badges_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Explorer_badges section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_Section_Sizes_Graph',
                        'label' => 'Section Size Graphs',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Section_Size_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Section_Size_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Section_Size_Graph section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_Section_Sizes',
                        'label' => 'Section Size Numbers',
                        'name' => 'OSM_Section_Sizes',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Section_Size_Numbers_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Section_Size_Numbers_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Section_Size_Numbers section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_Number_Of_Sections',
                        'label' => 'Number of Sections',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Number_Sections_' . $section_id,
		                'label' => 'Short Code',
		                'name' => 'OSM_Number_Sections_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Number_Of_Sections section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_membership_changes_graph',
                        'label' => 'Membership Charges',
                        'name' => '',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Membership_Changes_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Membership_Changes_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Membership_Changes section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_waitinglist_changes_graph',
                        'label' => 'Waiting List Changes',
                        'name' => 'OSM_waitinglist_changes_graph',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Waiting_List_Changes_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Waiting_List_Changes_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Waiting_List_Changes section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_leadership_changes_graph',
                        'label' => 'Leader Changes',
                        'name' => 'OSM_leadership_changes_graph',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Leadership_Changes_' . $section_id,
		                'label' => 'Short Code',
		                'name' => 'OSM_Leadership_Changes_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Leader_Changes section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_retention_graph',
                        'label' => 'Retention',
                        'name' => 'OSM_retention_graph',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Retention_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Retention_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Retention section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                    array(
                        'key' => 'OSM_number_events_graph',
                        'label' => 'Number of Events',
                        'name' => 'OSM_number_events_graph',
                        'type' => 'accordion',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'open' => 0,
                        'multi_expand' => 0,
                    ),
	                array(
		                'key' => 'OSM_Number_Of_Events_' . $section_id,
		                'label' => 'Short code',
		                'name' => 'OSM_Number_Of_Events_' . $section_id,
		                'type' => 'text',
		                'readonly' => 1,
		                'instructions' => '',
		                'required' => 0,
		                'conditional_logic' => 0,
		                'wrapper' => array(
			                'width' => '',
			                'class' => 'shortCodeCopy',
			                'id' => '',
		                ),
		                'default_value' => '[OSM_Number_Of_Events section-id="' . $section_id . '" section-code="' . $section_code . '"]',
		                'new_lines' => 'wpautop',
		                'esc_html' => 0,
	                ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 10,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
                'acfe_display_title' => '',
                'acfe_autosync' => '',
                'acfe_form' => 0,
                'acfe_meta' => '',
                'acfe_note' => '',
            ));

	        acf_add_local_field_group(array(
		        'key' => 'group_' . 'available_unfiltered_dashboard_reports',
		        'title' => 'Available Dashboard Reports (Unfiltered)',
		        'fields' => array(
			        array(
				        'key' => 'OSM_Member_ages_graph',
				        'label' => 'Member Ages',
				        'name' => 'OSM_Member_ages_graph',
				        'type' => 'accordion',
				        'instructions' => '',
				        'required' => 0,
				        'conditional_logic' => 0,
				        'wrapper' => array(
					        'width' => '',
					        'class' => '',
					        'id' => '',
				        ),
				        'placement' => 'top',
				        'endpoint' => 0,
				        'open' => 0,
				        'multi_expand' => 0,
			        ),
			        array(
				        'key' => 'OSM_Ages_Graph_' . $section_id,
				        'label' => 'Short code',
				        'name' => 'OSM_Ages_Graph_' . $section_id,
				        'type' => 'text',
				        'readonly' => 1,
				        'instructions' => '',
				        'required' => 0,
				        'conditional_logic' => 0,
				        'wrapper' => array(
					        'width' => '',
					        'class' => 'shortCodeCopy',
					        'id' => '',
				        ),
				        'default_value' => '[OSM_Member_Ages_Graph section-id="' . $section_id . '" section-code="' . $section_code . '"]',
				        'new_lines' => 'wpautop',
				        'esc_html' => 0,
			        ),
			        array(
				        'key' => 'OSM_Leaders_ages_graph',
				        'label' => 'Leader Ages',
				        'name' => 'OSM_Leaders_ages_graph',
				        'type' => 'accordion',
				        'instructions' => '',
				        'required' => 0,
				        'conditional_logic' => 0,
				        'wrapper' => array(
					        'width' => '',
					        'class' => '',
					        'id' => '',
				        ),
				        'placement' => 'top',
				        'endpoint' => 0,
				        'open' => 0,
				        'multi_expand' => 0,
			        ),
			        array(
				        'key' => 'OSM_Leader_Ages_' . $section_id,
				        'label' => 'Short code',
				        'name' => 'OSM_Leader_Ages_' . $section_id,
				        'type' => 'text',
				        'readonly' => 1,
				        'instructions' => '',
				        'required' => 0,
				        'conditional_logic' => 0,
				        'wrapper' => array(
					        'width' => '',
					        'class' => 'shortCodeCopy',
					        'id' => '',
				        ),
				        'default_value' => '[OSM_Leader_Ages section-id="' . $section_id . '" section-code="' . $section_code . '"]',
				        'new_lines' => 'wpautop',
				        'esc_html' => 0,
			        ),
		        ),
		        'location' => array(
			        array(
				        array(
					        'param' => 'options_page',
					        'operator' => '==',
					        'value' => $this->pageID,
				        ),
			        ),
		        ),
		        'menu_order' => 10,
		        'position' => 'normal',
		        'style' => 'default',
		        'label_placement' => 'top',
		        'instruction_placement' => 'field',
		        'hide_on_screen' => '',
		        'active' => true,
		        'description' => '',
		        'acfe_display_title' => '',
		        'acfe_autosync' => '',
		        'acfe_form' => 0,
		        'acfe_meta' => '',
		        'acfe_note' => '',
	        ));

        endif;
    }
}