<?php


class NeoWeb_Connector_Register_Resources_Page {

    private string $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 *
	 */
    public function __construct()
    {
	    $this->plugin_data = get_option('neoweb-connector-statistics-manager');
        $this->pageID = $this->get_plugin_data('productSlug') . '-resources';
    }

	public function prepareResourcesFields ($groupedSections, $groupedSectionsMetaData) {

		$groupAccordion = array();

        array_push($groupAccordion, array(
            'key' => $this->get_plugin_data('pluginSlug') . '_field_section_id_metaData',
            'label' => 'Data in this section is refreshed every 24 hours.<br/>Time left until next refresh:',
            'name' => $this->get_plugin_data('pluginSlug') . '_field_section_id_metaData',
            'type' => 'message',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => $groupedSectionsMetaData,
            'new_lines' => 'wpautop',
            'esc_html' => 0,
        ));

		foreach ($groupedSections as $groupID => $groupSections) {

			array_push($groupAccordion, array(
				'key' => $this->get_plugin_data('pluginSlug') . '_groupID_' . $groupID,
				'label' => $groupSections[0]['group_name'] . ' (Group ID: ' . $groupID . ')',
				'name' => $this->get_plugin_data('pluginSlug') . '_groupID_' . $groupID,
				'type' => 'accordion',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'open' => 0,
				'multi_expand' => 0,
				'endpoint' => 0,
			));

			foreach ($groupSections as $groupSection) {
				$sectionName = array(
					'key' => $this->get_plugin_data('pluginSlug') . '_field_section_id_' . $groupSection['section_id'],
					'label' => 'Section Name & ID',
					'name' => $this->get_plugin_data('pluginSlug') . '_field_section_id_' . $groupSection['section_id'],
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => 'sectionHeading',
						'id' => '',
					),
					'message' => $groupSection['section_name'] .' (Section ID: '. $groupSection['section_id'] .')',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				);

				switch ($groupSection['section_type']) {
					case "waiting" :
						$sectionTypeLabel =  "Waiting List";
						break;
					default:
						$sectionTypeLabel = ucfirst($groupSection['section_type']);
						break;
				}

				$sectionType = array(
					'key' => $this->get_plugin_data('pluginSlug') . '_field_section_id_' . $groupSection['section_id'] . '_sectionType_' . $groupSection['section_type'],
					'label' => 'Section Type',
					'name' => $this->get_plugin_data('pluginSlug') . '_field_section_id_' . $groupSection['section_id'] . '_sectionType_' . $groupSection['section_type'],
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => $sectionTypeLabel,
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				);

				//We have a waiting list do not create a term dates section
				array_push($groupAccordion,  $sectionName,
					$sectionType);

			}
		}

		$this->registerResourceFields($groupAccordion);
	}

	private function registerResourceFields($preparedResources) {

		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('pluginSlug') . 'neoweb_osm_oauth_resources_found',
				'title' => $this->get_plugin_data('pluginName') . ' - OSM Resources Found',
				'fields' => $preparedResources,
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
				'acfe_display_title' => '',
				'acfe_autosync' => '',
				'acfe_form' => 0,
				'acfe_meta' => '',
				'acfe_note' => '',
			));

		endif;

	}
}