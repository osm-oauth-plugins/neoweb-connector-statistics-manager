<?php


class NeoWeb_Connector_Register_Support_Page {

	private string $pageID;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 *
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-statistics-manager');
		$this->pageID = $this->get_plugin_data('productSlug') . '-support';

	}

	public function registerDefaultSupportFields () {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('productSlug') . 'general_support_info_group',
				'title' => 'System Specific Support Info',
				'fields' => array(
					array(
						'key' => $this->get_plugin_data('productSlug') . '_php_version',
						'label' => 'PHP Version:',
						'name' => $this->get_plugin_data('productSlug') . '_php_version',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => phpversion(),
						'new_lines' => '',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('productSlug') . 'wp_version',
						'label' => 'WordPress Version:',
						'name' => $this->get_plugin_data('productSlug') . 'wp_version',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => get_bloginfo('version'),
						'new_lines' => '',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'side',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;
	}
	public function registerPluginSupportFields () {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('pluginSlug') . 'support_info_group',
				'title' => $this->get_plugin_data('pluginName') . ' - Support Info',
				'fields' => array(
					array(
						'key' => $this->get_plugin_data('pluginSlug') . 'product_name',
						'label' => 'Product/Plugin Name:',
						'name' => $this->get_plugin_data('pluginSlug') . 'product_name',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => $this->get_plugin_data('pluginName'),
						'new_lines' => '',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_product_version',
						'label' => 'Product/Plugin Version:',
						'name' => $this->get_plugin_data('pluginSlug') . '_product_version',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => $this->get_plugin_data('pluginVersion'),
						'new_lines' => '',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_release_date',
						'label' => 'Product/Plugin Release Date:',
						'name' => $this->get_plugin_data('pluginSlug') . '_release_date',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => $this->get_plugin_data("pluginReleaseDate"),
						'new_lines' => '',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_support_email',
						'label' => 'Support Email:',
						'name' => $this->get_plugin_data('pluginSlug') . '_support_email',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<a href="mailto:' . $this->get_plugin_data("supportEmail") . '?subject=' . $this->get_plugin_data('pluginName') . " " . $this->get_plugin_data('pluginVersion') . '">' . $this->get_plugin_data('supportEmail') . '</a>',
						'new_lines' => '',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_support_link',
						'label' => 'Report Bugs / New Feature Requests:',
						'name' => $this->get_plugin_data('pluginSlug') . '_support_link',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<a target="_blank" href="'.$this->get_plugin_data('supportLink').'">'.$this->get_plugin_data('supportLink').'</a>',
						'new_lines' => '',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;
	}
}