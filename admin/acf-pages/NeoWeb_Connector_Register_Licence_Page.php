<?php


class NeoWeb_Connector_Register_Licence_Page {

    private string $pageID;

    private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * NeoWeb_Connector_Register_Licence_Page constructor.
	 *
	 */
    public function __construct()
    {
	    $this->plugin_data = get_option('neoweb-connector-statistics-manager');
        $this->pageID = $this->get_plugin_data('productSlug') . '-licences';

    }

	public function registerLicencePersonalDetailsFields () {
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . $this->get_plugin_data('productSlug') . '_request_key_personal_details_group',
				'title' => 'Personal Details',
				'fields' => array(
                    array(
                        'key' => $this->get_plugin_data('productSlug') . '_request_key_faq',
                        'label' => 'How to request your licence key and activate your plugin',
                        'name' => '',
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '<p>Please enter your personal details in the fields below. Click the "Save personal 
                            details" button to the right.</p>
                            <p>This will expose buttons at the bottom of the page to request 
                            and activate your licence key for this plugin.</p>
                            <p>The plugin will only load once this step has been completed.</p>',
                        'new_lines' => '',
                        'esc_html' => 0,
                    ),
					array(
						'key' => $this->get_plugin_data('productSlug') . '_first_name',
						'label' => 'First name',
						'name' => $this->get_plugin_data('productSlug') . '_first_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('productSlug') . '_last_name',
						'label' => 'Last name',
						'name' => $this->get_plugin_data('productSlug') . '_last_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('productSlug') . '_email_address',
						'label' => 'Email Address',
						'name' => $this->get_plugin_data('productSlug') . '_email_address',
						'type' => 'email',
						'instructions' => 'Please ensure this email address matched your PayPal email address to enable us to link your payment to your license key.',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
					),
					array(
						'key' => $this->get_plugin_data('productSlug') . '_organisation',
						'label' => 'Scout Group/District/County',
						'name' => $this->get_plugin_data('productSlug') . '_organisation',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('productSlug') . '_saved',
						'label' => 'Personal details saved correctly to Database?',
						'name' => $this->get_plugin_data('productSlug') . '_saved',
						'type' => 'text',
						'readonly' => 1,
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					)
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
				'acfe_display_title' => '',
				'acfe_autosync' => '',
				'acfe_form' => 0,
				'acfe_meta' => '',
				'acfe_note' => '',
			));

		endif;
	}

	public function registerLicenceProductDetailsFields () {

		$licenceManager = new NeoWeb_Connector_Licence_Manager();
		$licenceStatus = $licenceManager->getLicenceStatus();

		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_' . 'licenceGroup-' . $this->get_plugin_data('pluginSlug'),
				'title' => $this->get_plugin_data('pluginName') . ' - Version: ' . $this->get_plugin_data('pluginVersion'),
				'fields' => array(
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_licence_key',
						'label' => 'Licence Key',
						'name' => $this->get_plugin_data('pluginSlug') . '_licence_key',
						'type' => 'text',
						'readonly' => 1,
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_licence_status',
						'label' => 'Licence Status',
						'name' => $this->get_plugin_data('pluginSlug') . '_licence_status',
						'type' => 'text',
						'readonly' => 1,
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => ucfirst($licenceStatus),
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_request_licence',
						'label' => 'Request Licence Key',
						'name' => $this->get_plugin_data('pluginSlug') . '_request_licence',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array(
							array(
								array(
									'field' => $this->get_plugin_data('pluginSlug') . '_licence_key',
									'operator' => '==empty',
								),
								array(
									'field' => $this->get_plugin_data('productSlug') . '_saved',
									'operator' => '==',
									'value' => "Yes"
								),
							)
						),
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<button type="button" class="button-secondary" id="'.str_replace("-", "_", $this->get_plugin_data('pluginSlug')).'_register">Request Licence Key</button>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array(
						'key' => $this->get_plugin_data('pluginSlug') . '_activate_licence',
						'label' => 'Activate Licence Key',
						'name' => $this->get_plugin_data('pluginSlug') . '_activate_licence',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array(
							array(
								array(
									'field' => $this->get_plugin_data('pluginSlug') . '_licence_key',
									'operator' => '!=empty',
								),
								array(
									'field' => $this->get_plugin_data('pluginSlug') . '_licence_status',
									'operator' => '!=',
									'value' => 'Active',
								),
								array(
									'field' => $this->get_plugin_data('pluginSlug') . '_licence_status',
									'operator' => '!=',
									'value' => 'Expired',
								),
								array(
									'field' => $this->get_plugin_data('pluginSlug') . '_licence_status',
									'operator' => '!=',
									'value' => 'Blocked',
								)
							)
						),
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '<button type="button" class="button-secondary" id="'.str_replace("-", "_", $this->get_plugin_data('pluginSlug')).'_activate">Activate Licence Key</button>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => $this->pageID,
						)
					),
				),
				'menu_order' => 10,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'left',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;
	}
}