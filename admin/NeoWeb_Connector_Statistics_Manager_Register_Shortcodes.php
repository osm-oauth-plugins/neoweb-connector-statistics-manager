<?php


class NeoWeb_Connector_Statistics_Manager_Register_Shortcodes {

	private NeoWeb_Connector_Statistics_Manager_Auth_Caller $oAuthCaller;
	private NeoWeb_Connector_Loggers $logger;

	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __contructor
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_data = get_option('neoweb-connector-statistics-manager');

		$this->oAuthCaller = new NeoWeb_Connector_Statistics_Manager_Auth_Caller();
	}

	public function run_beaver_badges_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$section = "beavers";
		$transientID = 'beaver_badges_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionBadgeDrilldown;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $section, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['districts'][$availableDistrict]);

				$districtAverages = $reportDataJSON['districtaverages'];
				foreach ( $districtAverages['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['districtaverages']['categories'], $i, 1);
						foreach ( $districtAverages['series'] as $badgeID => $badgeData ) {
							//Remove the districts average
							array_splice($reportDataJSON['districtaverages']['series'][$badgeID]['data'], $i, 1);
						}
					}
				}
			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/section_badges_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_cub_badges_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$section = "cubs";
		$transientID = 'cubs_badges_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionBadgeDrilldown;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $section, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['districts'][$availableDistrict]);

				$districtAverages = $reportDataJSON['districtaverages'];
				foreach ( $districtAverages['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['districtaverages']['categories'], $i, 1);
						foreach ( $districtAverages['series'] as $badgeID => $badgeData ) {
							//Remove the districts average
							array_splice($reportDataJSON['districtaverages']['series'][$badgeID]['data'], $i, 1);
						}
					}
				}
			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/section_badges_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_scouts_badges_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$section = "scouts";
		$transientID = 'scouts_badges_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionBadgeDrilldown;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $section, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['districts'][$availableDistrict]);

				$districtAverages = $reportDataJSON['districtaverages'];
				foreach ( $districtAverages['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['districtaverages']['categories'], $i, 1);
						foreach ( $districtAverages['series'] as $badgeID => $badgeData ) {
							//Remove the districts average
							array_splice($reportDataJSON['districtaverages']['series'][$badgeID]['data'], $i, 1);
						}
					}
				}
			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/section_badges_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_explorers_badges_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$section = "explorers";
		$transientID = 'explorers_badges_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionBadgeDrilldown;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $section, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['districts'][$availableDistrict]);

				$districtAverages = $reportDataJSON['districtaverages'];
				foreach ( $districtAverages['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['districtaverages']['categories'], $i, 1);
						foreach ( $districtAverages['series'] as $badgeID => $badgeData ) {
							//Remove the districts average
							array_splice($reportDataJSON['districtaverages']['series'][$badgeID]['data'], $i, 1);
						}
					}
				}
			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/section_badges_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_section_size_graph_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'section_sizes_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionSizes;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON[$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/section_sizes_graph.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_section_size_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'section_sizes_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionSizes;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);


		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON[$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/section_sizes_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_number_of_sections_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'number_of_sections_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionNumbers;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['data'][$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/number_of_sections_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_member_ages_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'ages_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionAges;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/member_ages_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_leaders_ages_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'leader_ages_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionAgesLeaders;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/leaders_ages_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_membership_changes_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$type = "Membership Changes";
		$transientID = 'membership_changes_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getSectionGrowth;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['data'][$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/membership_changes_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_waitinglist_changes_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$type = "Waiting List Changes";
		$transientID = 'waitinglist_changes_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getWaitingListGrowth;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['data'][$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/membership_changes_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_leadership_changes_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$type = "Leadership Changes";
		$transientID = 'leadership_changes_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getLeaderGrowth;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['data'][$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/membership_changes_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_retention_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'retention_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getRetention;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
						foreach ( $reportDataJSON['series'] as $id => $data ) {
							//Remove the districts average
							array_splice($reportDataJSON['series'][$id]['data'], $i, 1);
						}
					}
				}
			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/retention_report.php');
		$html = ob_get_clean();

		return $html;

	}

	public function run_number_events_report ($attr) {

		$sectionID = $attr['section-id'];
		$sectionCode = $attr['section-code'];
		$transientID = 'number_events_report_'.$sectionID;
		$url = NeoWeb_Connector_Events_Manager_OSM_Endpoints::getNumberEvents;
		$formattedURL = (new NeoWeb_Connector_Events_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $sectionCode);

		$reportData = $this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 730, array());
		$reportDataJSON = json_decode($reportData, true);

		$districts = $this->oAuthCaller->getAvailableDistricts($sectionID);
		foreach ($districts as $availableDistrict => $districtData) {
			$included = get_field('district' . $this->get_plugin_data('pluginSlug') . '-settings' . str_replace(' ', '', $availableDistrict), 'option');
			if (!$included) {
				unset($reportDataJSON['data'][$availableDistrict]);

				foreach ( $reportDataJSON['categories'] as $i => $districtName ) {
					if ($districtName == $availableDistrict) {
						//Remove the district
						array_splice($reportDataJSON['categories'], $i, 1);
					}
				}

			}
		}

		ob_start();
		include( OSM_STATISTICS_PLUGIN_PATH . 'public/html-templates/number_events_report.php');
		$html = ob_get_clean();

		return $html;

	}

}