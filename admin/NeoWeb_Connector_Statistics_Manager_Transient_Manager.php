<?php


class NeoWeb_Connector_Statistics_Manager_Transient_Manager {

	private $prefix;

	/**
	 * __constructor.
	 *
	 * @param $prefix
	 */
	public function __construct( $prefix ) {
		$this->prefix = '_transient_' . $prefix . '_' ;
	}

	public function wds_delete_transients() {
		$this->wds_delete_transients_from_keys( $this->wds_search_database_for_transients_by_prefix() );
	}

	private function wds_search_database_for_transients_by_prefix() {
		global $wpdb;
		// Build up our SQL query
		$sql = "SELECT `option_name` FROM $wpdb->options WHERE `option_name` LIKE '%s'";
		// Execute our query
		$transients = $wpdb->get_results( $wpdb->prepare( $sql, $wpdb->esc_like($this->prefix) . '%' ), ARRAY_A );
		// If if looks good, pass it back
		if ( $transients && !is_wp_error( $transients ) ) {
			return $transients;
		}
		// Otherise return false
		return false;
	}

	private function wds_delete_transients_from_keys( $transients ) {
		if ( ! isset( $transients ) ) {
			return false;
		}
		// If we get a string key passed in, might as well use it correctly
		if ( is_string( $transients ) ) {
			$transients = array( array( 'option_name' => $transients ) );
		}
		// If its not an array, we can't do anything
		if ( ! is_array( $transients ) ) {
			return false;
		}
		$results = array();
		// Loop through our transients
		foreach ( $transients as $transient ) {
			if ( is_array( $transient ) ) {
				// If we have an array, grab the first element
				$transient = current( $transient );
			}
			// Remove that sucker
			$results[ $transient ] = delete_transient( str_replace( '_transient_', '', $transient ) );
		}
		// Return an array of total number, and number deleted
		return array(
			'total'   => count( $results ),
			'deleted' => array_sum( $results ),
		);
	}

}