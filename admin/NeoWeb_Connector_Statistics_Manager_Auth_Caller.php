<?php

/**
 *
 * See https://oauth2-client.thephpleague.com/usage/
 *
 */

require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/libs/3rd-party/autoload.php';

use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use mattcollins171\OAuth2\Client\Provider\Osm;

class NeoWeb_Connector_Statistics_Manager_Auth_Caller {

    /**
     * @var array
     */
    private array $osm_application_data;

    /**
     * @return array
     */
    public function getOsmApplicationData(): array
    {
        return $this->osm_application_data;
    }

    /**
     * @var string
     */
    private string $scope;

    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_slug    The string used to uniquely identify this plugin.
     */
    protected string $plugin_slug;


    private NeoWeb_Connector_Loggers $logger;
    private NeoWeb_Connector_Admin_Notifications $flashNotice;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * OSM_oAuth_Connector constructor
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-statistics-manager');
        /*
        $administration = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_administration', 'option') != "none" ? "section:administration:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_administration', 'option') : "");
        $badge = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_badge', 'option') != "none" ? "section:badge:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_badge', 'option') : "");
        $event = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_event', 'option') != "none" ? "section:event:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_event', 'option') : "");
        $finance = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_finance', 'option') != "none" ? "section:finance:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_finance', 'option') : "");
        $flexirecord = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_flexirecord', 'option') != "none" ? "section:flexirecord:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_flexirecord', 'option') : "");
        $member = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_member', 'option') != "none" ? "section:member:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_member', 'option') : "");
        $programme = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_programme', 'option') != "none" ? "section:programme:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_programme', 'option') : "");
        $quartermaster = (get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_quartermaster', 'option') != "none" ? "section:quartermaster:" . get_field($this->get_plugin_data('pluginSlug') . '_osm_permissions_quartermaster', 'option') : "");
        $this->scope = $administration . " " . $badge . " " . $event . " " . $finance . " " . $flexirecord . " " . $member . " " . $programme . " " . $quartermaster;
		*/

        $this->scope = "";

		$this->osm_application_data['clientId'] = get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_client_id", "option");
		$this->osm_application_data['clientSecret'] = get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_secret", "option");
		$this->osm_application_data['redirectUri'] = get_admin_url() . "admin.php?page=neoweb-connector-app-settings";
		$this->osm_application_data['urlAuthorize'] = NeoWeb_Connector_Statistics_Manager_OSM_Endpoints::authorize;
		$this->osm_application_data['urlAccessToken'] = NeoWeb_Connector_Statistics_Manager_OSM_Endpoints::token;
		$this->osm_application_data['urlResourceOwnerDetails'] = NeoWeb_Connector_Statistics_Manager_OSM_Endpoints::resources;
		$this->osm_application_data['scope'] = $this->getScope();

		$this->logger = new NeoWeb_Connector_Loggers(
			plugin_dir_path( dirname( __FILE__ ) )
		);

        $this->flashNotice = new NeoWeb_Connector_Admin_Notifications();
	}

	public function osm_authenticate(): array
    {
		$provider = new Osm($this->getOsmApplicationData());

		// If we don't have an authorization code then get one
		if (!isset($_GET['code'])) {

			$options = ['scope' => $this->getScope()];
			// Get authorization URL
			$authorizationUrl = $provider->getAuthorizationUrl($options);

			// Get the state generated for you and store it to the session.
			$_SESSION['oauth2state'] = $provider->getState();

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $authorizationUrl);
            $response = curl_exec($ch);
            curl_close($ch);

            $json = json_decode($response, true);

            if (isset($json['error'])) {
                // Failed to get through to OSM using the return address - probably not added as an app url.
                return array(
                    "status"  =>"error",
                    "message" => $json['message']
                );

            } else if (isset($_GET['error_description'])) {
                //Scope probably failed
                return array(
                    "status"  =>"error",
                    "message" => $_GET['error_description'] . " : " . $_GET['hint']
                );

            } else {

                $this->flashNotice->add_flash_notice("Authentication with OSM has been attempted, to check if successful 
				please click  'Authenticate Plugin' again to complete the authorisation process",
                    "success", false);

                // Redirect the user to the authorization URL.
                header('Location: ' . $authorizationUrl, true);
                exit;
            }

        // Check given state against previously stored one to mitigate CSRF attack
		} elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

			if (isset($_SESSION['oauth2state'])) {
				unset($_SESSION['oauth2state']);
			}

			return array(
				"status"=>"error",
				"message"=>"Authentication failed. Invalid State Sent."
			);

		} else {

			try {

				// Try to get an access token using the authorization code grant.
				$accessToken = $provider->getAccessToken('authorization_code', [
					'code' => $_GET['code']
				]);

				//Store the access token, for use later on
				update_option($this->get_plugin_data('pluginSlug') . "_osm_access_token_data", $accessToken);
				update_field($this->get_plugin_data('pluginSlug') . "_accessToken", $accessToken, 'option');

				$resourceOwner = $provider->getResourceOwner($accessToken);
				$resourceOwnerData = $resourceOwner->toArray();

				update_option($this->get_plugin_data('pluginSlug') . "_osm_userData_userID", $resourceOwnerData['data']['user_id']);
				update_option($this->get_plugin_data('pluginSlug') . "_osm_userData_full_name", $resourceOwnerData['data']['full_name']);
				update_option($this->get_plugin_data('pluginSlug') . "_osm_userData_email", $resourceOwnerData['data']['email']);

				return array(
					"status"=>"success",
					"message"=>"OSM has been successfully authenticated."
				);

			} catch (IdentityProviderException $e) {

				// Failed to get the access token or user details.
				return array(
					"status"  =>"error",
					"message" => $e->getMessage()
				);
			}
		}
	}

	public function get_access_token () {
		$provider = new Osm($this->getOsmApplicationData());

		//Get the current access token
		$existingAccessToken = get_option($this->get_plugin_data('pluginSlug') . "_osm_access_token_data");

        if (!empty($existingAccessToken)) {
            if ($existingAccessToken->hasExpired()) {
                try {

                    $newAccessToken = $provider->getAccessToken('refresh_token', [
                        'refresh_token' => $existingAccessToken->getRefreshToken()
                    ]);
                    //Save the new access token, replacing the old one
                    update_option($this->get_plugin_data('pluginSlug') . "_osm_access_token_data", $newAccessToken);
                    update_field($this->get_plugin_data('pluginSlug') . "_accessToken", $newAccessToken, 'option');
                    $existingAccessToken = $newAccessToken;

                } catch (IdentityProviderException $e) {
                    var_dump($e);
                }

                if (empty($newAccessToken))
                    $this->osm_authenticate();
            }
        }

		return $existingAccessToken;
	}

	public function get_available_resources(): array {

		$osmUserID = get_option($this->get_plugin_data('pluginSlug') . '_osm_userData_userID');

		$uuid = $this->get_plugin_data('pluginSlug') . '_osm_'. $osmUserID . "_resourceData";
		$response = get_transient($uuid);

		$readTransient = true;
		if( false === $response ) {
			// Transient expired, refresh the data
			$provider = new Osm( $this->getOsmApplicationData() );

			$accessToken = $this->get_access_token();

			// Using the access token, we may look up details about the
			// resource owner.
			$resourceOwner = $provider->getResourceOwner( $accessToken );
			$response = $resourceOwner->toArray();

			if (empty(trim($response['error']))) {
				//Only set the transient if we were successful
				set_transient( $uuid, $response, 24 * HOUR_IN_SECONDS );
			}

			$readTransient = false;

		}

		if (get_field($this->get_plugin_data('pluginSlug') .'_enable_api_call_logs', 'option')) {
			$this->logger->transient_logger("get_available_resources", "", "", $response, $readTransient);
		}
		return $response;

    }

    public function testCallAPI ($url, $args = array()) {
	    $accessToken = $this->get_access_token();
	    $args = array(
		    'method' => 'POST',
		    'headers'     => array(
			    'Authorization' => 'Bearer ' . $accessToken,
			    'Content-Type' => 'application/x-www-form-urlencoded'
		    ),
	    );

	    $response = wp_remote_post( $url, $args );

	    update_field($this->get_plugin_data('pluginSlug') . '_maxRateLimit', wp_remote_retrieve_header( $response, 'X-RateLimit-Limit' ), 'option');
	    update_field($this->get_plugin_data('pluginSlug') . '_currentRateLimit', wp_remote_retrieve_header( $response, 'X-RateLimit-Remaining' ), 'option');
	    $time = date("m/d/Y h:i:s",time() + wp_remote_retrieve_header( $response, 'X-RateLimit-Reset' ));
	    update_field($this->get_plugin_data('pluginSlug') . '_rateLimitRefresh',  $time, 'option');

        return $response;
    }

    public function osmAPICaller ($transientID, $url, $hours = 12, $parts = array()): string {

	    $url_components = parse_url($url);
	    parse_str($url_components['query'], $params);

	    if (get_field('currentRateLimit', 'option') > 0 || empty(get_field('currentRateLimit', 'option'))) {

		    $maxRateLimit = get_field($this->get_plugin_data('pluginSlug') . '_maxRateLimit', 'option');
		    $remainingRateLimit = get_field($this->get_plugin_data('pluginSlug') . '_currentRateLimit', 'option');
		    $rateLimitReset = get_field($this->get_plugin_data('pluginSlug') . '_rateLimitRefresh', 'option');

		    //Has the reset time passed, if yes we can reset our rate limit
		    if (new DateTime() > new DateTime($rateLimitReset)) {
			    $remainingRateLimit = $maxRateLimit;
		    }

		    $accessToken = $this->get_access_token();
		    $args = array(
			    'method' => 'POST',
			    'headers'     => array(
				    'Authorization' => 'Bearer ' . $accessToken,
				    'Content-Type' => 'application/x-www-form-urlencoded'
			    ),
			    'body' => $parts
		    );

		    $userid = get_option($this->get_plugin_data('pluginSlug') . "_osm_userData_userID");
		    $response = "";
		    if ($transientID != false) {
			    $uuid = $this->get_plugin_data('pluginSlug') . '_osm_'. $userid . "_" . $transientID;
			    $response = get_transient($uuid);
			    $readTransient = true;
			    if( false === $response ) {
				    // Transient expired, refresh the data
				    if ($remainingRateLimit > 0 || empty($remainingRateLimit)) {
					    $response = wp_remote_post( $url, $args );

					    update_field($this->get_plugin_data('pluginSlug') . '_maxRateLimit', wp_remote_retrieve_header( $response, 'X-RateLimit-Limit' ), 'option');
					    update_field($this->get_plugin_data('pluginSlug') . '_currentRateLimit', wp_remote_retrieve_header( $response, 'X-RateLimit-Remaining' ), 'option');
					    $time = date("m/d/Y h:i:s",time() + wp_remote_retrieve_header( $response, 'X-RateLimit-Reset' ));
					    update_field($this->get_plugin_data('pluginSlug') . '_rateLimitRefresh',  $time, 'option');

					    if (wp_remote_retrieve_response_code($response) == 429) {
						    $message = __('ERROR: ' . wp_remote_retrieve_response_message($response));
						    new WPSE_Error_Message($message);
					    } else if (wp_remote_retrieve_response_code($response) == 200) {
						    set_transient($uuid, $response, $hours * HOUR_IN_SECONDS);
					    }
					    $readTransient = false;
				    }
			    }

			    if (get_field($this->get_plugin_data('pluginSlug') .'_enable_api_call_logs', 'option')) {
				    $this->logger->transient_logger("perform_osm_api_call_" . $params['action'], $url, $args, $response, $readTransient);
			    }

		    } else {
			    if ($remainingRateLimit > 0 || empty($remainingRateLimit)) {
				    $response = wp_remote_post( $url, $args );

				    update_field($this->get_plugin_data('pluginSlug') . '_maxRateLimit', wp_remote_retrieve_header( $response, 'X-RateLimit-Limit' ), 'option');
				    update_field($this->get_plugin_data('pluginSlug') . '_currentRateLimit', wp_remote_retrieve_header( $response, 'X-RateLimit-Remaining' ), 'option');
				    $time = date("m/d/Y h:i:s",time() + wp_remote_retrieve_header( $response, 'X-RateLimit-Reset' ));
				    update_field($this->get_plugin_data('pluginSlug') . '_rateLimitRefresh',  $time, 'option');

				    if (get_field($this->get_plugin_data('pluginSlug') .'_enable_api_call_logs', 'option')) {
					    $this->logger->transient_logger("perform_osm_api_call_" . $params['action'], $url, $args, $response, false);
				    }
			    }
		    }

		    return wp_remote_retrieve_body( $response );

	    } else {
		    echo ('OSM API has not been authorised or we have run out of API calls');
	    }
    }

    public function getTransientMetaData($transientID): string
    {
        $time_left = time();
        $userid = get_option($this->get_plugin_data('pluginSlug') . "_osm_userData_userID");
        if ($transientID != false) {
            $uuid = $this->get_plugin_data('pluginSlug') . '_osm_' . $userid . "_" . $transientID;
            $expires = (int) get_option( '_transient_timeout_' . $uuid, 0 );
            $time_left = $expires - time();
        }
        return $this->timeLeft($time_left);
    }

    private function timeLeft($secs): string
    {
        $bit = array(
            ' year'        => $secs / 31556926 % 12,
            ' week'        => $secs / 604800 % 52,
            ' day'        => $secs / 86400 % 7,
            ' hour'        => $secs / 3600 % 24,
            ' minute'    => $secs / 60 % 60,
            ' second'    => $secs % 60
        );

        foreach($bit as $k => $v){
            if($v > 1)$ret[] = $v . $k . 's';
            if($v == 1)$ret[] = $v . $k;
        }
        array_splice($ret, count($ret)-1, 0, 'and');
        $ret[] = '';

        return join(' ', $ret);
    }

	private function groupBy($arr, $criteria, $type = "all") {
		return array_reduce($arr, function($accumulator, $item) use ($criteria, $type) {
			$key = (is_callable($criteria)) ? $criteria($item) : $item[$criteria];

			switch ($type) {
				case "discount" :
					//Only return sections that have a type of "discount"
					if ($this->isDiscount($item['section_id'])) {
						if (!array_key_exists($key, $accumulator)) {
							$accumulator[$key] = [];
						}
						array_push($accumulator[$key], $item);
					}
					break;

				case "waiting" :
					//Only return sections that have a type of "waiting"
					if ($this->isWaitingList($item['section_id'])) {
						if (!array_key_exists($key, $accumulator)) {
							$accumulator[$key] = [];
						}
						array_push($accumulator[$key], $item);
					}
					break;

				case "sections" :
					//Only return sections that does not have a type of "waiting" or "discount"
					if (!$this->isWaitingList($item['section_id']) || !$this->isDiscount($item['section_id'])) {
						if (!array_key_exists($key, $accumulator)) {
							$accumulator[$key] = [];
						}
						array_push($accumulator[$key], $item);
					}
					break;

				default :
					if (!array_key_exists($key, $accumulator)) {
						$accumulator[$key] = [];
					}
					array_push($accumulator[$key], $item);
					break;
			}

			return $accumulator;
		}, []);
	}

    public function getAvailableSections() {
        $resources = $this->get_available_resources();
        return $resources['data']['sections'];
    }

	public function getResourcesByGroup() {
		$resources = $this->get_available_resources();
		$sections = $resources['data']['sections'];

		//Group the sections by the ID they belong too
		return $this->groupBy($sections, 'group_id');
	}

	public function getAvailableCounties() {
		$resources = $this->get_available_resources();
		$sections = $resources['data']['sections'];

		//Group the sections by the ID they belong too
		return $this->groupBy($sections, 'group_id', 'discount');
	}

	public function getAvailableDistricts($county) {
		$transientID = 'district_data_'.$county;
		$url = NeoWeb_Connector_Statistics_Manager_OSM_Endpoints::getDistricts;
		$formattedURL = (new NeoWeb_Connector_Statistics_Manager_OSM_Endpoints)->formatEndPoint($url, $county);
		$districts = json_decode($this->osmAPICaller($transientID, $formattedURL, 8730, array()), true);

		return $districts['districts'];
	}

	public function getAvailableWaitingLists() {
		$resources = $this->get_available_resources();
		$sections = $resources['data']['sections'];

		//Group the sections by the ID they belong too
		return $this->groupBy($sections, 'group_id', 'waiting');
	}

	public function getAvailableSectionsByGroup() {
		$resources = $this->get_available_resources();
		$sections = $resources['data']['sections'];

		//Group the sections by the ID they belong too
		return $this->groupBy($sections, 'group_id', 'sections');
	}

    public function getAvailableTermsPerSection($section_id) {
        $sections = $this->getAvailableSections();
        foreach ($sections as $section) {
            if ($section['section_id'] == $section_id)
                return $section['terms'];
        }
    }

	public function getCurrentTermID($section_id) {
		$dateToday = strtotime("now");
		$term_id = "";

		$terms = $this->getAvailableTermsPerSection($section_id);
		if (is_array($terms) || is_object($terms)) {
			foreach ($terms as $term ) {
				if ( $dateToday >= strtotime( $term['startdate'] ) && $dateToday <= strtotime( $term['enddate'] ) ) {
					$term_id = $term['term_id'];
					break;
				}
			}

			return $term_id;
		}
	}

    public function getSectionType($section_id) {
        $sections = $this->getAvailableSections();
        foreach ($sections as $section) {
            if ($section['section_id'] == $section_id)
                return $section['section_type'];
        }
    }

	public function isWaitingList($section_id): bool {
		if ($this->getSectionType($section_id) == "waiting") {
			return true;
		} else {
			return false;
		}
	}

	public function isDiscount($section_id): bool {
		if ($this->getSectionType($section_id) == "discount") {
			return true;
		} else {
			return false;
		}
	}

    public function getSectionName($section_id) {
        $sections = $this->getAvailableSections();
        foreach ($sections as $section) {
            if ($section['section_id'] == $section_id)
                return $section['section_name'];
        }
    }

    public function getGroupName($section_id) {
        $sections = $this->getAvailableSections();
        foreach ($sections as $section) {
            if ($section['section_id'] == $section_id)
                return $section['group_name'];
        }
    }

    public function getGroupID($section_id) {
        $sections = $this->getAvailableSections();
        foreach ($sections as $section) {
            if ($section['section_id'] == $section_id)
                return $section['group_id'];
        }
    }

}