<?php

/**
 *
 * @link              https://neoweb.co.uk
 * @since             1.0.0
 * @package           NeoWeb_Connector_Statistics_Manager
 *
 * @wordpress-plugin
 * Product Name:            NeoWeb Connector
 * Product Slug:            neoweb-connector
 * Plugin Name:             NeoWeb Connector - Statistics Manager
 * Plugin URI:              https://scoutsuk.org
 * Description:             Online Scout Manager (OSM) WordPress oAuth Connector and Cache Manager for country and district dashboards
 * Version:                 1.0.0
 * Release Date:            17/01/2021
 * Requires at least:       5.2
 * Requires PHP:            7.2
 * Author:                  Jaco Mare
 * Author URI:              https://neoweb.co.uk
 * Copyright:               Jaco Mare
 * Text Domain:             neoweb-connector-statistics-manager
 * Domain Path:             /languages
 * Support Email:           support@neoweb.co.uk
 * BitBucket Support Link:  https://bitbucket.org/osm-oauth-plugins/neoweb-connector-statistics-manager/issues?status=new&status=open
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

update_option('neoweb-connector-statistics-manager', get_file_data(__FILE__, array(
    'productName'       => 'Product Name',
    'productSlug'       => 'Product Slug',
    'pluginName'        => 'Plugin Name',
    'pluginSlug'        => 'Text Domain',
    'pluginVersion'     => 'Version',
    'pluginReleaseDate' => 'Release Date',
    'supportEmail'      => 'Support Email',
    'supportLink'       => 'BitBucket Support Link'
), false));


$pluginData = get_option('neoweb-connector-statistics-manager');
require_once plugin_dir_path( __FILE__ ) . 'includes/libs/pluginUpdateChecker/plugin-update-checker.php';
$pluginUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://scoutsuk.org/plugin-updater/'.$pluginData['pluginSlug'].'.json',
    __FILE__,
    $pluginData['pluginSlug']
);

define('OSM_STATISTICS_PLUGIN_PATH', plugin_dir_path( __FILE__ ));

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/NeoWeb_Connector_Statistics_Manager_Activator.php
 */
function activate_neoweb_connector_statistics_manager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/NeoWeb_Connector_Statistics_Manager_Activator.php';
	NeoWeb_Connector_Statistics_Manager_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/NeoWeb_Connector_Statistics_Manager_Deactivator.php
 */
function deactivate_neoweb_connector_statistics_manager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/NeoWeb_Connector_Statistics_Manager_Deactivator.php';
	NeoWeb_Connector_Statistics_Manager_Deactivator::deactivate();
}

/**
 * The code that runs during plugin update.
 * This action is documented in includes/NeoWeb_Connector_Statistics_Manager_updatetor.php
 */
function update_neoweb_connector_statistincs_manager($upgrader_object, $funcOptions) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/NeoWeb_Connector_Statistics_Manager_updatetor.php';
	NeoWeb_Connector_Statistics_Manager_updatetor::update($upgrader_object, $funcOptions);
}
add_action( 'upgrader_process_complete', 'update_neoweb_connector_statistics_manager', 10, 2);

register_activation_hook( __FILE__, 'activate_neoweb_connector_statistics_manager' );
register_deactivation_hook( __FILE__, 'deactivate_neoweb_connector_statistics_manager' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/NeoWeb_Connector_Statistics_Manager.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @param $pluginData
 *
 * @since    1.0.0
 */
function run_neoweb_connector_statistics_manager($pluginData) {

	$plugin = new NeoWeb_Connector_Statistics_Manager();
	$plugin->run();

}
run_neoweb_connector_statistics_manager($pluginData);
