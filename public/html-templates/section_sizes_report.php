

<div id="container-SectionSize-<?php echo $sectionID; ?>">

</div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb Statistics Connector Manager</a></small></p>

<script type="application/javascript">
    var jsonString = <?php echo json_encode($reportDataJSON); ?>;

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function buildTable (district, data_group) {

        var doc = document;

        var fragment = doc.createDocumentFragment();

        var heading = doc.createElement("h3");
        heading.className = "districtHeading";
        heading.innerText = district;

        var subHeading = doc.createElement("p");
        subHeading.className = "districtSubText";
        subHeading.innerText = 'Current size is ' + data_group.numscouts + '. Ideal size is ' + data_group.idealsize;

        var thead = doc.createElement("thead");

        var td = doc.createElement("th");
        td.innerHTML = "Group/Section";
        thead.appendChild(td);

        var td = doc.createElement("th");
        td.innerHTML = "Section Type";
        thead.appendChild(td);

        var td = doc.createElement("th");
        td.innerHTML = "Size";
        thead.appendChild(td);

        var td = doc.createElement("th");
        td.innerHTML = "Ideal Size";
        thead.appendChild(td);

        //does not trigger reflow
        fragment.appendChild(thead);

        var tbody = doc.createElement("tbody");

        for(var sub_group in data_group.byGroup) {
            var group = data_group.byGroup[sub_group];
            if(!group.idealsize) {
                group.idealsize = '0';
            }
            if(!group.numscouts) {
                group.numscouts = '0';
            }

            var tr = doc.createElement("tr");

            var td = doc.createElement("td");
            td.innerHTML = sub_group;
            tr.appendChild(td);

            var td = doc.createElement("td");
            tr.appendChild(td);

            var td = doc.createElement("td");
            td.innerHTML = group.numscouts;
            tr.appendChild(td);

            var td = doc.createElement("td");
            td.innerHTML = group.idealsize;
            tr.appendChild(td);

            tbody.appendChild(tr);

            for(var index in group.sections) {
                var section = group.sections[index];

                var tr = doc.createElement("tr");

                var td = doc.createElement("td");
                td.innerHTML = section.name;
                tr.appendChild(td);

                var td = doc.createElement("td");
                td.innerHTML = capitalizeFirstLetter(section.type);
                tr.appendChild(td);

                var td = doc.createElement("td");
                td.innerHTML = section.numscouts;
                tr.appendChild(td);

                var td = doc.createElement("td");
                td.innerHTML = section.idealsize;
                tr.appendChild(td);

                tbody.appendChild(tr);
            }
        }

        fragment.appendChild(tbody);

        var table = doc.createElement("table");
        table.className = "table";
        table.appendChild(fragment);

        var districtContainer = doc.createElement("div")
        districtContainer.className = "districtContainer";
        districtContainer.appendChild(heading);
        districtContainer.appendChild(subHeading);
        districtContainer.appendChild(table);

        return districtContainer;
    }


    for(var district in jsonString) {
        if(district && jsonString[district].byGroup) {
            var panel = buildTable(district, jsonString[district]);
        }
        jQuery('#container-SectionSize-<?php echo $sectionID; ?>').append(panel);
    }



</script>
