

<div id="container-SectionBadges-<?php echo $sectionID; ?>" style="width:100%; height:400px;"></div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb Statistics Connector Manager</a></small></p>

<script type="application/javascript">

    var j = <?php echo json_encode($reportDataJSON); ?>;

    var chart;
    var chartOptions = {
        chart: {
            renderTo: "container-SectionBadges-<?php echo $sectionID; ?>",
            type: 'column'
        },
        xAxis: {
            categories: [],
            labels: {
                rotation: 90,
                align: 'top',
                style: 'font-weight: bold'
            }
        },
        title: {
            text: 'District comparison'
        },
        subtitle: {
            text: 'Percentage of members who have completed the badges'
        },
        yAxis: {
            min: 0,
            title: {
                text: '%'
            }
        },
        legend: {
            layout: 'vertical',
            backgroundColor: '#FFFFFF',
            align: 'left',
            verticalAlign: 'top'
        },
        tooltip: {
            formatter: function() {
                if (this.series.type == "column") {
                    return this.series.name + ': '+ this.y +'%';
                } else {
                    return "Average " + this.series.name;
                }
            }
        },
        series: [],
        plotOptions: {
            column: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            if (!chart.drilledDown) {
                                chart.drilledDown = true;
                                var district = this.category;
                                chart.setTitle({text: district});
                                chart.xAxis[0].setCategories(chart.originalJson['districts'][district]['categories']);
                                var max = chart.series.length;
                                for (var i = 0; i < max; i++) {
                                    chart.series[0].remove();
                                }
                                for (var i in chart.originalJson['districts'][district]['series']) {
                                    chart.addSeries(chart.originalJson['districts'][district]['series'][i]);
                                }
                            } else {
                                chart.drilledDown = false;
                                chart.setTitle({text:"District comparison"});
                                var district = this.category;
                                chart.xAxis[0].setCategories(chart.originalJson['districtaverages']['categories']);
                                var max = chart.series.length;
                                for (var i = 0; i < max; i++) {
                                    chart.series[0].remove();
                                }
                                for (var shortname in chart.originalJson['districtaverages']['series']) {
                                    var series = chart.originalJson['districtaverages']['series'][shortname];
                                    series.lineWidth = 5;
                                    series.groupPadding = 0.05;
                                    chart.addSeries(series);
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    if (typeof j['districtaverages'] !== 'undefined') {
        chartOptions.xAxis.categories = j['districtaverages']['categories'];
        for (var shortname in j['districtaverages']['series']) {
            var series = j['districtaverages']['series'][shortname];
            series.lineWidth = 5;
            series.groupPadding = 0.05;
            chartOptions.series.push(series);
        }
    }


    chart = new Highcharts.Chart(chartOptions);
    chart.originalJson = j;



</script>
