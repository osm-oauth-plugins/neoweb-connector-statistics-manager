

<div id="container-SectionSizeGraph-<?php echo $sectionID; ?>" style="width:100%; height:400px;"></div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb Statistics Connector Manager</a></small></p>

<script type="application/javascript">
    var jsonString = <?php echo json_encode($reportDataJSON); ?>;

    var chart;
    var columns = [];
    columns.push({name: 'Ideal capactiy', key: 'idealsize', color: '#4572A7'});
    columns.push({name: 'Actual capacity', key: 'numscouts', color: '#AA4643'});
    columns.push({name: 'Joining list (all ages)', key: 'waiting', color: '#89A54E'});
    var chartOptions = {
        chart: {
            renderTo: "container-SectionSizeGraph-<?php echo $sectionID; ?>",
            type: 'column'
        },
        xAxis: {
            categories: [],
            labels: {
                rotation: 90,
                align: 'top',
                style: 'font-weight: bold'
            }
        },
        title: {
            text: 'Area Overview'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of members'
            }
        },
        legend: {
            layout: 'vertical',
            backgroundColor: '#FFFFFF',
            align: 'left',
            verticalAlign: 'top'
        },
        tooltip: {
            formatter: function() {
                return this.series.name + ': '+ this.y;
            }
        },
        series: [],
        plotOptions: {
            column: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            if (chart.graphLevel == 1) {
                                chart.graphLevel = 2;
                                var district = this.category;

                                chart.district = district;
                                chart.setTitle({text: district});
                                var categories = [];
                                for (var groupName in chart.originalJson[district]['byGroup']) {
                                    categories.push(groupName);
                                }
                                chart.xAxis[0].setCategories(categories);
                                var max = chart.series.length;
                                for (var i = 0; i < max; i++) {
                                    chart.series[0].remove();
                                }
                                for (var k in columns) {
                                    var series = {};
                                    series.name = columns[k].name;
                                    series.color = columns[k].color;
                                    series.data = [];
                                    for (var i in chart.originalJson[district]['byGroup']) {
                                        if (chart.originalJson[district]['byGroup'][i][columns[k].key]) {
                                            series.data.push(chart.originalJson[district]['byGroup'][i][columns[k].key]);
                                        } else {
                                            series.data.push(0);
                                        }
                                    }
                                    chart.addSeries(series);
                                }
                            } else if (chart.graphLevel == 2) {
                                chart.graphLevel = 3;
                                var group = this.category;
                                var district = chart.district;
                                chart.setTitle({text: group});
                                var categories = [];
                                if (chart.originalJson[district]['byGroup'][group]['sections']) {
                                    for (var i in chart.originalJson[district]['byGroup'][group]['sections']) {
                                        categories.push(chart.originalJson[district]['byGroup'][group]['sections'][i].name);
                                    }
                                }
                                chart.xAxis[0].setCategories(categories);
                                var max = chart.series.length;
                                for (var i = 0; i < max; i++) {
                                    chart.series[0].remove();
                                }
                                for (var k in columns) {
                                    var series = {};
                                    if (columns[k].key != 'waiting') {
                                        series.name = columns[k].name;
                                    } else {
                                        series.name = 'Waiting list (right age)';
                                    }
                                    series.color = columns[k].color;
                                    series.data = [];
                                    if (chart.originalJson[district]['byGroup'][group]['sections']) {
                                        for (var i in chart.originalJson[district]['byGroup'][group]['sections']) {
                                            series.data.push(chart.originalJson[district]['byGroup'][group]['sections'][i][columns[k].key]);
                                        }
                                        chart.addSeries(series);
                                    }
                                }
                            } else if (chart.graphLevel == 3) {
                                chart.graphLevel = 1;
                                var max = chart.series.length;
                                for (var i = 0; i < max; i++) {
                                    chart.series[0].remove();
                                }
                                chart.setTitle({text: "Area Overview"});
                                chart.xAxis[0].setCategories(chart.originalJson['categories']);

                                for (var k in columns) {
                                    var series = {};
                                    series.name = columns[k].name;
                                    series.color = columns[k].color;
                                    series.data = [];
                                    for (var i in chart.originalJson['categories']) {
                                        var district = chart.originalJson['categories'][i];
                                        series.data.push(chart.originalJson[district][columns[k].key]);
                                    }
                                    chart.addSeries(series);
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    chartOptions.xAxis.categories = jsonString['categories'];

    for (var k in columns) {
        var series = {};
        series.name = columns[k].name;
        series.color = columns[k].color;
        series.data = [];
        for (var i in jsonString['categories']) {
            var district = jsonString['categories'][i];
            series.data.push(jsonString[district][columns[k].key]);
        }
        chartOptions.series.push(series);
    }

    chart = new Highcharts.Chart(chartOptions);
    chart.originalJson = jsonString;
    chart.graphLevel = 1;

</script>
