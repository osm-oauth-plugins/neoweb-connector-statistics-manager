

<div id="container-retention-<?php echo $sectionID; ?>" style="width:100%; height:400px;"></div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb Statistics Connector Manager</a></small></p>

<script type="application/javascript">
    var jsonString = <?php echo json_encode($reportDataJSON); ?>;
    console.log(jsonString);

    var columns = [];
    columns.push({name: 'Beavers', key: 'beavers', color: '#0099FF'});
    columns.push({name: 'Cubs', key: 'cubs', color: '#33CC00'});
    columns.push({name: 'Scouts', key: 'scouts', color: '#006666'});
    columns.push({name: 'Explorers', key: 'explorers', color: '#999966'});
    columns.push({name: 'Adults', key: 'adults', color: '#CC9966'});

    var chart = new Highcharts.Chart({
        chart: {
            type: 'bar',
            renderTo: 'container-retention-<?php echo $sectionID; ?>'
        },
        title: {
            text: 'Retention Percentage'
        },
        xAxis: {
            categories: jsonString.categories
        },
        colors: ['#0099FF', '#33CC00', '#006666', '#999966'],
        yAxis: {
            min: 0,
            title: {
                text: 'Retention Percentage'
            }
        },
        legend: {
            align: 'right',
            x: -100,
            verticalAlign: 'top',
            y: 20,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '%<br/>';
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {}
            }
        },
        series: jsonString.series
    });

</script>
