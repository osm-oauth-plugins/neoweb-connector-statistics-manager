


<div id="container-MemberAges-<?php echo $sectionID; ?>" style="width:100%; height:400px;"></div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb Statistics Connector Manager</a></small></p>

<script type="application/javascript">
    var jsonString = <?php echo json_encode($reportDataJSON); ?>;

    console.log(jsonString);

    var columns = [];
    columns.push({name: 'Beavers', key: 'beavers', color: '#0099FF'});
    columns.push({name: 'Cubs', key: 'cubs', color: '#33CC00'});
    columns.push({name: 'Scouts', key: 'scouts', color: '#006666'});
    columns.push({name: 'Explorers', key: 'explorers', color: '#999966'});
    columns.push({name: 'Adults', key: 'adults', color: '#CC9966'});
    columns.push({name: 'Waiting List', key: 'waiting', color: 'grey'});

    var chartOptions = {
        chart: {
            renderTo: "container-MemberAges-<?php echo $sectionID; ?>",
            type: 'column'
        },
        xAxis: {
            categories: [],
            title: {
                text: 'Age'
            },
            labels: {
                rotation: 90,
                align: 'top',
                style: 'font-weight: bold'
            }
        },
        title: {
            text: 'Members Age Distribution'
        },
        yAxis: {
            min: 0,
            allowDecimals: false,
            title: {
                text: 'No. Members'
            }
        },
        legend: {
            layout: 'vertical',
            backgroundColor: '#FFFFFF',
            align: 'left',
            verticalAlign: 'top'
        },
        tooltip: {
            formatter: function() {
                return this.series.name + ': '+ this.y;
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: []
    };
    chartOptions.xAxis.categories = [];
    for (var k in jsonString.allages) {
        chartOptions.xAxis.categories.push(jsonString.allages[k]);
    }

    for (var k in columns) {
        var series = {};
        series.name = columns[k].name;
        series.color = columns[k].color;
        series.data = [];
        for (var l in jsonString.allages) {
            if (jsonString.data[columns[k].key]) {
                if (jsonString.data[columns[k].key][jsonString.allages[l]]) {
                    series.data.push(jsonString.data[columns[k].key][jsonString.allages[l]]);
                } else {
                    series.data.push(0);
                }
            }
        }

        chartOptions.series.push(series);
    }

    var chart = new Highcharts.Chart(chartOptions);
    chart.originalJson = jsonString;
    chart.graphLevel = 1;


</script>
