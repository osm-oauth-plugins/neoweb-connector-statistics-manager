<?php


class NeoWeb_Connector_Statistics_Manager_updatetor {

	public static function update ($upgrader_object, $funcOptions) {

		if ( !function_exists( 'get_plugins' ) ){
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		$plugin_data = get_option("neoweb-connector-statistics-manager");

		if( $funcOptions['action'] == 'update' && $funcOptions['type'] == 'plugin' && isset( $funcOptions['plugins'] ) ) {
			// Iterate through the plugins being updated and check if ours is there
			foreach( $funcOptions['plugins'] as $plugin ) {
				if( $plugin == $plugin_data['pluginName'] ) {

					$headers = "Content-Type: text/html; charset=UTF-8" .PHP_EOL;

					$to = $plugin_data['supportEmail'];
					$subject = "Site Updated:" . $plugin_data['pluginName'];

					ob_start();

					include(plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/register-plugin-email-template.php');
					$htmlMessage = ob_get_clean();

					wp_mail( $to, $subject, $htmlMessage, $headers);

				}
			}
		}

	}

}