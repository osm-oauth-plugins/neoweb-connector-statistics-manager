<?php

if( !class_exists('acf') ) {
	define( 'NEOWEB_STATISTICS_ACF_PATH', plugin_dir_path( dirname( __FILE__ ) ) . 'includes/libs/acf/' );
	define( 'NEOWEB_STATISTICS_ACF_URL', plugin_dir_url( dirname( __FILE__ ) ) . 'includes/libs/acf/' );

	// Include the ACF plugin.
	include_once( NEOWEB_STATISTICS_ACF_PATH . 'acf.php' );

	// Customize the url setting to fix incorrect asset URLs.
	add_filter( 'acf/settings/url', 'neoweb_statistics_settings_url' );
	function neoweb_statistics_settings_url( $url ) {
		return NEOWEB_STATISTICS_ACF_URL;
	}

	// (Optional) Hide the ACF admin menu item.c
	add_filter( 'acf/settings/show_admin', 'neoweb_statistics_show_admin' );
	function neoweb_statistics_show_admin( $show_admin ) {
		return true;
	}
}